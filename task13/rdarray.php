<?php

define("ROOT_PATH", dirname(__FILE__, 2));
$file = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "task13" . DIRECTORY_SEPARATOR . "usersdata.txt", "r");
$check_file = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task13" . DIRECTORY_SEPARATOR . "usersdata.txt");

$usersarray = [];
$flag = 0;

if ($check_file) {

    while (!feof($file)) {
        $words = fgets($file);
        $words_line = explode(' ', $words);

        $usersarray[$flag]["name"] = $words_line[0];
        $usersarray[$flag]["login"] = $words_line[1];
        $usersarray[$flag]["password"] = $words_line[2];
        $usersarray[$flag]["email"] = $words_line[3];
        $usersarray[$flag]["lang"] = $words_line[4];

        $flag++;
    }
}

sort($usersarray);