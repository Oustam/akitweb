<?php

define("ROOT_PATH", dirname(__FILE__, 2));

$check_Data = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . "logindata.json");
$check_Numb = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . $_POST["login"] . ".json");

if ($check_Data) {
    $contentData = file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . "logindata.json");
    $arrayUsers = json_decode($contentData, true);
}

if ($check_Numb == false) {
    foreach ($arrayUsers as $user) {
        if ($user["login"] == $_POST["login"] and $user["password"] == $_POST["login-password"]) {
            $putContNum = "{\"numOfTruInp\":0}";
            file_put_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . $_POST["login"] . ".json", $putContNum);
            $check_Numb = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . $_POST["login"] . ".json");
        }
    }
}

if ($check_Data and $check_Numb) {
    $contentNumb = file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . $_POST["login"] . ".json");
    $correctInput = json_decode($contentNumb, true);

    foreach ($arrayUsers as $user) {
        if ($user["login"] == $_POST["login"] and $user["password"] == $_POST["login-password"]) {
            echo $_POST["login"];
            $correctInput["numOfTruInp"]++;
            file_put_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "task12json" . DIRECTORY_SEPARATOR . $_POST["login"] . ".json", json_encode($correctInput));
        }
    }
}