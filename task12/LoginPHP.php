<?php

define("ROOT_PATH", dirname(__FILE__, 2));

$file = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "task12" . DIRECTORY_SEPARATOR . "logindata.txt", "r");
$dataInput = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "task12" . DIRECTORY_SEPARATOR . $_POST["login"] . ".txt", "r+");

$check_file = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task12" . DIRECTORY_SEPARATOR . "logindata.txt");
$check_dataInput = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task12" . DIRECTORY_SEPARATOR . $_POST["login"] . ".txt");

$flag = 0;

if ($check_file and $check_dataInput) {


    while (!feof($file)) {
        $words = fgets($file);
        $words_line = explode(' ', $words);
        if ($words_line[0] == $_POST["login"] and $words_line[1] == $_POST["login-password"]) {
            echo $_POST["login"];

            if (filesize($_POST["login"] . ".txt") > 0) {
                $strdatainput = fread($dataInput, filesize($_POST["login"] . ".txt"));
                $itemPosition = strpos($strdatainput, ":");
                $flag_line = substr($strdatainput, $itemPosition + 1);

                $flag = (int)$flag_line;
                rewind($dataInput);
            }

            $flag++;

            if ($dataInput) {
                fwrite($dataInput, "Кількість правильних спроб входу для користувача " . $_POST["login"] . ": " . $flag);
                rewind($dataInput);
            }
        }
    }
}