<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

define("ROOT_PATH", dirname(__FILE__));

define("DB_HOST", "localhost");
define("DB_NAME", "bdfield");
define("DB_USER", "db_user");
define("DB_USER_PASS", "root123");

session_start();

require_once ROOT_PATH . DIRECTORY_SEPARATOR . "function.php";

try {
    $dbConn = new PDO("mysql:host=" . DB_HOST . ";dbname=" . DB_NAME, DB_USER, DB_USER_PASS);
    $dbConn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    //echo "Successfully connected with myDB database";
} catch (Exception $e) {
    echo "Connection failed" . $e->getMessage();
}
