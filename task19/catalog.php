<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "task19" . DIRECTORY_SEPARATOR . "config.php";

$TestArray = showProductsList($dbConn);

if (empty($_SESSION["email"])) {
    header("Location: ReloForm.php");
    die();
}

if (!empty($_SESSION["email"]) and !empty($_POST["product"])) {

    addProductToCart($dbConn, $_POST["product"], $_SESSION["email"]);
    header("Location: cart.php");
    die();
}

?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Catalog</title>
    <link rel="stylesheet" href="css/styleCa.css">
</head>

<body class="body">

    <div class="frame">
        <form action="catalog.php" method="POST">
            <h2>Вибір товару</h2>
            <hr>

            <?php

            foreach ($TestArray as $product) {
                $productDB = $product["name"];
                $flagA = 1;
                $flagB = "product" . $flagA;
                echo "<div class='row'>";
                echo "<label for='$flagB'>$productDB</label><br>";
                echo "<input type='checkbox' name='product[]' value='$productDB' id='$flagB'>";
                echo "</div>";
                $flagA++;
            }

            ?>

            <br>
            <div class="submit">
                <input type="submit" name="submit" value="Замовити">
            </div>

        </form>

    </div>


</body>

</html>