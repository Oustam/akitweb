<?php
require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "task19" . DIRECTORY_SEPARATOR . "config.php";

if (empty($_SESSION["email"])) {
    header("Location: ReloForm.php");
    die();
}

if (!empty($_SESSION["email"])) {

$cartDataArray = showUserCart($dbConn, $_SESSION["email"]);

}


?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Cart</title>
    <link rel="stylesheet" href="css/styleRC.css">
</head>

<body class="body">

    <form>

        <div class="styleTable">
            <table>
                <tbody>
                    <tr>
                        <td>
                            <h2>Кошик товарів</h2>
                            <hr>

                            <?php
                            foreach ($cartDataArray as $Data) {
                                echo "Ім'я товару - " . $Data["name"] . ", " . "ціна - " . $Data["price"] * $Data["quantity"] . " грн, " . "кількість - " . $Data["quantity"] . "<br>";
                            }
                            ?>

                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

    </form>

</body>

</html>