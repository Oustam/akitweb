<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "task20" . DIRECTORY_SEPARATOR . "config.php";

$TestArray = showProductsList($dbConn);

if (empty($_SESSION["email"])) {
  header("Location: ReloForm.php");
  die();
}

if (!empty($_SESSION["email"]) and !empty($_POST["product"])) {

  addProductToCart($dbConn, $_POST["product"], $_SESSION["email"]);
  header("Location: cart.php");
  die();
}

?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Catalog Form</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/js/bootstrap.bundle.min.js" integrity="sha384-qKXV1j0HvMUeCBQ+QVp7JcfGl760yU08IQ+GpUo5hlbpg51QRiuqHAJz8+BrxE/N" crossorigin="anonymous"></script>

  <style>
    .bd-placeholder-img {
      font-size: 1.125rem;
      text-anchor: middle;
      -webkit-user-select: none;
      -moz-user-select: none;
      user-select: none;
    }

    @media (min-width: 768px) {
      .bd-placeholder-img-lg {
        font-size: 3.5rem;
      }
    }

    .text-muted {
      color: white !important;
    }

    div.new {
      display: flex;
      justify-content: center;
      align-items: center;
      padding-top: 20px;
    }

    div.new input[type="submit"] {
      text-align: center;
      flex-basis: 200px;
    }

    .card-body {
      height: 85px;
    }

    section.py-5.text-center.container {
      max-width: 100%;
      background-image: url("background.jpg");
      background-repeat: no-repeat;
      background-position: center;
      background-size: cover;
    }

    div.col-lg-6.col-md-8.mx-auto {
      background-color: rgba(0, 0, 0, 0.6);
      border: 1px solid #6f97b0;
      max-width: 500px;
    }

    .fw-light {
      color: white;
    }
  </style>
</head>

<body>

  <header>
    <div class="collapse bg-dark" id="navbarHeader">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-7 py-4">
            <h4 class="text-white">Про сайт</h4>
            <p class="text-muted">Some shop - невеликий інтернет магазин, який представляє каталог різних товарів...</p>
          </div>
        </div>
      </div>
    </div>
    <div class="navbar navbar-dark bg-dark shadow-sm">
      <div class="container">
        <a href="#" class="navbar-brand d-flex align-items-center">
          <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" fill="currentColor" class="me-2" viewBox="0 0 16 16">
            <path d="M8 1a2 2 0 0 1 2 2v2H6V3a2 2 0 0 1 2-2zm3 4V3a3 3 0 1 0-6 0v2H3.36a1.5 1.5 0 0 0-1.483 1.277L.85 13.13A2.5 2.5 0 0 0 3.322 16h9.355a2.5 2.5 0 0 0 2.473-2.87l-1.028-6.853A1.5 1.5 0 0 0 12.64 5H11zm-1 1v1.5a.5.5 0 0 0 1 0V6h1.639a.5.5 0 0 1 .494.426l1.028 6.851A1.5 1.5 0 0 1 12.678 15H3.322a1.5 1.5 0 0 1-1.483-1.723l1.028-6.851A.5.5 0 0 1 3.36 6H5v1.5a.5.5 0 1 0 1 0V6h4z" />
          </svg>

          <strong>Some shop</strong>
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
    </div>
  </header>

  <main>

    <section class="py-5 text-center container">
      <div class="row py-lg-5">
        <div class="col-lg-6 col-md-8 mx-auto">
          <h1 class="fw-light">Каталог товарів</h1>
          <p class="lead text-muted">Наш магазин пропонує різноманітні товари високої якості, які відповідають ринковим цінам. Приємних покупок!</p>

        </div>
      </div>
    </section>

    <div class="album py-5 bg-light">
      <div class="container">
        <form action="Newcatalog.php" method="POST">
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">

            <?php

            foreach ($TestArray as $product) {
              $productDB = $product["name"];
              $productPrice = $product["price"];
              $productPrice = round($productPrice, 0);
              $productImage = $product["image"];
              $flagA = 1;
              $flagB = "product" . $flagA;
              echo "<div class='col'>\n";
              echo "<div class='card shadow-sm'>\n";
              echo "<img class='bd-placeholder-img card-img-top' src='$productImage' height='225' width='299.328px'>\n";


              echo "<div class='card-body'>\n";
              echo "<p class='card-text'>" . $productDB . ".</p>\n";
              echo "<div class='d-flex justify-content-between align-items-center'>\n";
              echo "<div class='form-check'>\n";
              echo "<label class='form-check-label' for='$flagB'> додати в кошик</label>\n";
              echo "<input class='form-check-input' type='checkbox' id='$flagB' name='product[]' value='$productDB'>\n";
              echo "</div>\n<small>" . $productPrice . " грн</small>\n";
              echo "</div>\n</div>\n</div>\n</div>\n";
              $flagA++;
            }

            ?>

          </div>

          <div class="new">
            <input type="submit" class="btn btn-primary btn-block mb-3" value="Замовити">
          </div>
        </form>
      </div>
    </div>



  </main>

</body>

</html>