<?php

function checkRememberMe()
{
    if (empty($_SESSION["email"])) {
        if (!empty($_COOKIE["email"])) {
            $_SESSION["email"] = $_COOKIE["email"];
        } else {
            header("Location: ReloForm.php");
            die();
        }
    }
}

function checkLogin(PDO $dbConnect, string $email, string $password): array|bool
{
    if (empty($email) || empty($password)) {
        return [];
    }
    $stmt = $dbConnect->prepare("SELECT * FROM `users` WHERE email = :email AND password = :password");
    $stmt->execute(["email" => $email, "password" => $password]);
    $user = $stmt->fetch();
    return $user;
}

function checkEmail(PDO $dbConnect, string $email)
{
    $stmt = $dbConnect->prepare("SELECT * FROM `users` WHERE email = :email");
    $stmt->execute(["email" => $email]);
    $user = $stmt->fetch();
    return $user;
}

function registerUser(PDO $dbConnect, array $userData)
{
    $stmt = $dbConnect->prepare(
        "INSERT INTO `users` 
            (
                `name`, 
                `email`, 
                `password`
            ) 
        VALUES 
            (
                :name, 
                :email, 
                :password
            )"
    );
    $stmt->execute(
        [
            "name" => $userData["first_name"],
            "email" => $userData["rEmail"],
            "password" => $userData["rPassword"]

        ]
    );
    return true;
}

function showProductsList(PDO $dbConnect)
{
    $stmt = $dbConnect->query("SELECT `name`, `price`, `image` FROM products");
    $db_products = $stmt->fetchAll();
    return $db_products;
}

function addProductToCart(PDO $dbConnect, array $UserProducts, $UserEmail)
{
    foreach ($UserProducts as $productName) {
        // збільшує кількість товару, якщо він був у кошику
        $stmt = $dbConnect->prepare(
            "UPDATE `carts` 
        SET quantity = quantity + 1
        WHERE user_id IN (SELECT `user_id` FROM `users` WHERE `email` = :email) AND product_id IN (SELECT `product_id` FROM `products` WHERE `name` = :name)"
        );
        $stmt->execute(
            [
                "email" => $UserEmail,
                "name" => $productName
            ]
        );

        // поміщає товар у кошик, якщо його там не було
        $stmt = $dbConnect->prepare(
            "INSERT IGNORE INTO `carts` 
                (
                    `user_id`, 
                    `product_id`, 
                    `quantity`, 
                    `date_order`
                )
            SELECT 
                (
                    SELECT `user_id` 
                    FROM `users` 
                    WHERE `email` = :email
                ),
            
                (
                    SELECT 
                    `product_id` 
                    FROM `products` 
                    WHERE `name` = :name
                ),

                '1', NOW()
            
            WHERE 
                NOT EXISTS 
                (
                    SELECT 1
                    FROM `carts`
                    WHERE `user_id` = (SELECT `user_id` FROM `users` WHERE `email` = :email)
                    AND `product_id` = (SELECT `product_id` FROM `products` WHERE `name` = :name)
                )"
        );

        $stmt->execute(
            [
                "email" => $UserEmail,
                "name" => $productName
            ]
        );
    }
    return true;
}




function showUserCart(PDO $dbConnect, $UserEmail)
{
    $stmt = $dbConnect->prepare(
    "SELECT products.name, products.price, carts.quantity
    FROM carts
    INNER JOIN users ON carts.user_id = users.user_id
    INNER JOIN products ON carts.product_id = products.product_id
    WHERE users.user_id in (SELECT `user_id` FROM `users` WHERE `email` = :email)"
    );

    $stmt->execute(
        [
            "email" => $UserEmail

        ]
    );
    $resultData = $stmt->fetchAll();
    return $resultData;
}