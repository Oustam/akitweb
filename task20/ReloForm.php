<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "task20" . DIRECTORY_SEPARATOR . "config.php";

if (!empty($_POST) and array_key_exists("first_name", $_POST)) {
    $fields = ["first_name", "rEmail", "rPassword", "confirm-password"];
    $errors = [];
    foreach ($fields as $field) {
        if (empty($_POST[$field])) {
            $errors[$field] = "Field " . $field . " must be filled";
        }
    }
    if (!filter_var($_POST["rEmail"], FILTER_VALIDATE_EMAIL)) {
        $errors["email"] = "Email is not valis";
    }
    if (!empty(checkEmail($dbConn, $_POST['rEmail']))) {
        $errors["email"] = "Email is is already existed";
    }
    if ($_POST['rPassword'] !== $_POST['confirm-password']) {
        $errors["confirm-password"] = "Password was not's confirmed";
    }
    if (empty($errors)) {
        registerUser($dbConn, $_POST);
        $_SESSION['email'] = $_POST['rEmail'];
        header("Location: Newcatalog.php");
        die();
    }

    echo "<pre>";
    print_r($errors);
    echo "</pre>";
    die();
}

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    if (!empty(checkLogin($dbConn, $_POST['email'], $_POST['password']))) {
        $_SESSION['email'] = $_POST['email'];
        if (!empty($_POST['remember_me'])) {
            setcookie("email", $_POST['email'], time() + 3600 * 24); //lisnee
        }

        header("Location: Newcatalog.php");
        die();
    }
}

include ROOT_PATH . DIRECTORY_SEPARATOR . "ReLoForm.html";
