<?php
$date = "31-12-2020";
$dateNumb = explode("-", $date);
$finalDate = $dateNumb[2] . "." . $dateNumb[1] . "." . $dateNumb[0];
echo $finalDate;

echo "<br><hr><br>";

$london = "london is the capital of great britain";

$London = ucwords($london);
echo $London;

echo "<br><hr><br>";

$password = "heresome123";
$nub = mb_strlen($password);

if ($nub > 7 && $nub < 12) {
    echo "Пароль підходить";
} else {
    echo "Потрібно вигадати інший пароль";
}

echo "<br><hr><br>";

$wrongText = "1a2b3c4b5d6e7f8g9h0";
$vowels = array("1", "2", "3", "4", "5", "6", "7", "8", "9", "0");
$correctText = str_replace($vowels, "", $wrongText);

echo $correctText;

echo "<br><hr><br>";

//$text = "Головним фактором мови РНР є практичність. РНР має надати програмісту кошти для швидкого та ефективного вирішення поставлених завдань. Практичний характер РНР обумовлений п'ятьма важливими характеристиками: традиційністю, простотою, ефективністю, безпекою, гнучкістю. Існує ще одна «характеристика», яка робить РНР особливо привабливим: він поширюється безкоштовно!Причому, з відкритими вихідними кодами (Open Source) Мова РНР здаватиметься знайомим програмістам, що працюють у різних галузях.Багато конструкцій мови запозичені з Сі, Perl. у типових програмах на С або Pascal Це помітно знижує початкові зусилля при вивченні РНР PHP — мова, що поєднує переваги Perl і Сі і спеціально націлена на роботу в Інтернеті, мова з універсальною (щоправда, за деякими застереженнями) та ясним синтаксисом. PHP є досить молодою мовою, він набув такої популярності серед web-програмістів, що на даний момент є чи не найпопулярнішою мовою для створення web-додатків (скриптів).";
$text = "The main factor of the RPR language is practicality. The RPR should provide the programmer with the means to quickly and effectively solve the tasks. The practical nature of PNR is determined by five important characteristics: traditionality, simplicity, efficiency, security, flexibility. There is another characteristic that makes PHP particularly attractive: it is distributed free of charge! Moreover, with open source codes (Open Source), the PHP language will seem familiar to programmers working in various industries. Many language structures are borrowed from C, Perl. in typical programs in C or Pascal. This significantly reduces the initial effort when learning PHP, a language that combines the advantages of Perl and C and is specifically aimed at working on the Internet, a language with a universal (although with some caveats) and clear syntax. PHP is a fairly young language, it has gained such popularity among web programmers that at the moment it is almost the most popular language for creating web applications (scripts).";

$words = explode(" ", $text);
$newText = "";
$countWords = count($words);
$finalText = "";
$arfin = [];
$akey = 0;
$lastText = "";
$reText = "";

for ($i = 0; $i < $countWords; $i++) {
    $word = $words[$i];
    $newText .= $word;
    if (mb_strlen($newText) == 80) {
        $finalText = $newText;
        $length = 80;

        $arfin["$akey"]["string"] = $finalText;
        $arfin["$akey"]["length"] = $length;

        $newText = "";
        $akey++;
    } elseif (mb_strlen($newText) > 80) {
        $i--;
        $newText = substr_replace($newText, "", -mb_strlen($word));
        $length = mb_strlen($newText) - 1;

        $finalText = $newText;

        $arfin["$akey"]["string"] = $finalText;
        $arfin["$akey"]["length"] = $length;
        $newText = "";
        $akey++;
    } else {
        $newText .= " ";
    }
}

$lastline = $newText;
$countAr = count($arfin);

for ($j = 0; $j < $countAr; $j++) {
    if ($arfin["$j"]["length"] == 80) {
        $lastText .= $arfin["$j"]["string"];
        $lastText .= "<br>";
    } else {
        $reText = $arfin["$j"]["string"];
        $words = explode(" ", $reText);
        $countWords = count($words);
        $rest = 80 - $arfin["$j"]["length"];
        for ($l = 0; $l < $countWords; $l++) {
            $word = $words[$l];
            $newText .= $word;
            $lastText .= $word;
            if ($rest > 0) {
                $lastText .= " &nbsp;";
                $rest--;
            } else {
                $lastText .= " ";
            }
        }
        $lastText .= "<br>";
    }
}

$lastText .= $lastline;
echo "Відформатований текст:<br>";
echo $lastText;