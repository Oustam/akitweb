<?php
session_start();

define("ROOT_PATH", dirname(__FILE__, 2));

$check_file = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task14" . DIRECTORY_SEPARATOR . "data.txt");

$cartProd = "";

if ($check_file) {
    $file = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "task14" . DIRECTORY_SEPARATOR . "data.txt", "r");

    foreach ($_SESSION["products"] as $prod) {

        while (!feof($file)) {
            $words = fgets($file);
            $words_line = explode(";", $words);
            if ($words_line[0] == $prod) {
                $cartProd .= "Ім'я товару - " . $words_line[0] . ", " . "ціна - " . $words_line[1] . ", " . "кількість - " . $words_line[2] . "<br>";

            }
        
        }

        rewind($file);

    }
}



if (empty($_SESSION["products"])) {
    header("Location: catalog.php");
    die();
}

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Cart</title>
    <link rel="stylesheet" href="css/styleRC.css">
</head>

<body class="body">

    <form action="LoginPHP.php" method="POST">

        <div class="styleTable">
            <table>
            <tbody>
            <tr>
                <td>
                    <h2>Кошик товарів</h2>
                    <hr>

                    <?php echo $cartProd;?>



                </td>
            </tr>
            </tbody>
            </table>
        </div>

    </form>

</body>
</html>
