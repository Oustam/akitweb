<?php

session_start();

define("ROOT_PATH", dirname(__FILE__, 2));
$check_file = file_exists(ROOT_PATH . DIRECTORY_SEPARATOR . "task14" . DIRECTORY_SEPARATOR . "data.txt");

if ($check_file) {
    $file = fopen(ROOT_PATH . DIRECTORY_SEPARATOR . "task14" . DIRECTORY_SEPARATOR . "data.txt", "r");
    $wordArr = [];
    
    while (!feof($file)) {
        $wordSet = fgets($file);
        $words_line = explode(";", $wordSet);
        array_push($wordArr, $words_line[0]);
    }
    rewind($file);
}

if( ! empty($_POST["submit"]))
{

    $dataList = "";

    foreach($_POST["product"] as $item) {
        $dataList .= $item . ";";    
    }

    $words = explode(";", $dataList);
    array_pop($words);

    $_SESSION["products"] = $words;
    
    header("Location: cart.php");
    die();
}

?>


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Catalog</title>
    <link rel="stylesheet" href="css/styleCa.css">
</head>

<body class="body">

<div class="frame">
		<form action="catalog.php" method="POST">
			<h2>Вибір товару</h2>
    <hr>

        <?php
        foreach ($wordArr as $product) {
            $flagA = 1;
            $flagB = "product" . $flagA;
            echo "<div class='row'>";
            echo "<label for='$flagB'>$product</label><br>";
            echo "<input type='checkbox' name='product[]' value='$product' id='$flagB'>";
            echo "</div>";
            $flagA++;
        }
        ?>

    <br>
        <div class="submit">  
		    <input type="submit" name="submit" value="Замовити">
        </div>  

		</form>

	</div>


</body>
</html>