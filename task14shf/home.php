<?php

declare(strict_types=1);
require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "task14shf" . DIRECTORY_SEPARATOR . "config.php";

$key = "fasdwsadase";
$cipherMethod = "AES-128-ECB";

if (empty($_SESSION['email']) and (!empty($_COOKIE['email']))) {

    if (str_contains($_COOKIE['email'], ":")) {
        //розпакування cookie
        $cookieContainer = explode(":", $_COOKIE['email']);
    } else {
        header("Location: login.php");
        die();
    }

    $cookieContainer[0]; // email
    $cookieContainer[1]; // підпис

    //розшифровка підпису
    $decrypted = openssl_decrypt($cookieContainer[1], $cipherMethod, $key);

    //якщо структура підпису введена неправильно
    if (!$decrypted){
        header("Location: login.php");
        die();
    }

    //перевірка відповідності підпису та емейлу
    if ($cookieContainer[0] === $decrypted) {
        $_SESSION['email'] = $cookieContainer[0];
    } else {
        header("Location: login.php");
        die();
    }
}

if (empty($_SESSION['email']) and (empty($_COOKIE['email']))) {

    header("Location: login.php");
    die();
}

if ($_SESSION['email'] === "admin@admin.com") {
    echo "Admin Panel Here!" . ".<br>";
} else {
    echo "Hello, " . $_SESSION['email'] . ".<br>";
}

echo "Welcome!";
