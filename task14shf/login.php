<?php

require_once dirname(__FILE__, 2) . DIRECTORY_SEPARATOR . "task14shf" . DIRECTORY_SEPARATOR . "config.php";
$key = "fasdwsadase";
$cipherMethod = "AES-128-ECB";

if (!empty($_POST['email']) && !empty($_POST['password'])) {
    if ($_POST['email'] === USER_EMAIL && $_POST['password'] === USER_PASSWORD) {
        $_SESSION['email'] = $_POST['email'];
        if (!empty($_POST['remember_me'])) {

            $signature = openssl_encrypt($_POST['email'], $cipherMethod, $key);
            $emailAndcipher = $_POST['email'] . ":" . $signature;

            setcookie("email", $emailAndcipher, time() + 3600 * 24);
        }

        header("Location: home.php");
        die();
    }
}

include ROOT_PATH . DIRECTORY_SEPARATOR . "login.html";
